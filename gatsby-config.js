/**
 * @prettier
 */
let activeEnv = process.env.ACTIVE_ENV;

if (!activeEnv) {
    activeEnv = 'development';
}

require('dotenv').config({path: `.env.${activeEnv}`});

const config = require('./data/SiteConfig');
const pathPrefix = config.pathPrefix === '/' ? '' : config.pathPrefix;

module.exports = {
    pathPrefix: config.pathPrefix,
    siteMetadata: {
        siteUrl: config.site.url + pathPrefix
    },
    plugins: [
        'gatsby-plugin-sass',
        'gatsby-plugin-react-helmet',
        {
            resolve: 'gatsby-plugin-google-analytics',
            options: {
                head: true,
                respectDNT: true,
                trackingId: config.tracking.googleAnalyticsID
            }
        }
    ]
};
