# Coming Soon

A static &#39;coming soon&#39; page, used to get traffic and encourage newsletter sign-up before the launch of the full site.