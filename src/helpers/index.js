import {withPrefix} from 'gatsby-link';

export const imagePath = (filename, folder = '/images/') => `${folder}${filename}`;
 
export const importImage = filename => withPrefix(imagePath(filename));

export const isMobile = () => {
    if (typeof window !== 'undefined') {
        const layout = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content');
        
        return layout;
    }

    return false;
};
