/**
 * @prettier
 */
import React, {Component, Fragment} from 'react';
import Helmet from 'react-helmet';
import config from '../../data/SiteConfig';
import SiteFooter from '../components/SiteFooter';
import '../styles/index.scss';

class MainLayout extends Component {
    render() {
        const {children} = this.props;
        const path = this.props.location.pathname.replace('/', '');
        const slug = path === '' ? 'home' : path;
        const classes = {js: typeof window !== 'undefined' ? 's-js' : 's-no-js'};

        return (
            <Fragment>
                <Helmet
                    htmlAttributes={{
                        class: `s-${slug} ${classes.js}`,
                        lang: 'en'
                    }}
                >
                    <title>{`${config.site.title}`}</title>
                    <meta name="description" content={config.site.description} />
                    <link rel="shortcut icon" href="/images/images/favicon.ico" />
                    <link
                        rel="canonical"
                        href={`${config.site.url}${this.props.location.pathname}`}
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="57x57"
                        href="/images/apple-icon-57x57.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="60x60"
                        href="/images/apple-icon-60x60.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="72x72"
                        href="/images/apple-icon-72x72.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="76x76"
                        href="/images/apple-icon-76x76.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="114x114"
                        href="/images/apple-icon-114x114.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="120x120"
                        href="/images/apple-icon-120x120.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="144x144"
                        href="/images/apple-icon-144x144.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="152x152"
                        href="/images/apple-icon-152x152.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        sizes="180x180"
                        href="/images/apple-icon-180x180.png"
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="192x192"
                        href="/images/android-icon-192x192.png"
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="32x32"
                        href="/images/favicon-32x32.png"
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="96x96"
                        href="/images/favicon-96x96.png"
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="16x16"
                        href="/images/favicon-16x16.png"
                    />
                </Helmet>
                <div className="c-site-wrapper">{children()}</div>
            </Fragment>
        );
    }
}

export default MainLayout;
