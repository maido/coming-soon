/**
 * @prettier
 */
import React, {Component, Fragment} from 'react';
import {easing, tween} from 'popmotion';
import posed from 'react-pose';
import config from '../../data/SiteConfig';
import Logo from '../components/Logo';
import NewsletterSignupForm from '../components/NewsletterSignupForm';
import SEO from '../components/SEO';

const INITIAL_DELAY = 800;
const ANIMATIONS = {
    hidden: {
        opacity: 0,
        translateY: 30
    },
    visible: {
        opacity: 1,
        transition: props => tween({...props, duration: 800, easing: easing.circOut}),
        translateY: 0
    }
};
const AnimatedLogo = posed.div(ANIMATIONS);
const AnimateBody = posed.div({
    hidden: {...ANIMATIONS.hidden, ...{translateY: 20}},
    visible: ANIMATIONS.visible
});

class Subscribed extends Component {
    constructor(props) {
        super(props);

        this.state = {animateBody: false, animateLogo: false};
    }

    componentDidMount() {
        setTimeout(() => this.setState({animateLogo: true}), 300);
        setTimeout(() => this.setState({animateBody: true}), 500);
    }

    render() {
        const {animateBody, animateLogo} = this.state;

        return (
            <Fragment>
                <SEO />
                <section className="o-wrapper u-max-w-400 u-text-center">
                    <div className="u-max-w-400 u-m-auto">
                        <AnimatedLogo
                            pose={animateLogo ? 'visible' : 'hidden'}
                            className="u-js-anim"
                        >
                            <Logo className="c-site-logo u-mb" />
                        </AnimatedLogo>
                    </div>

                    <AnimateBody pose={animateBody ? 'visible' : 'hidden'} className="u-js-anim">
                        Thank you for signing up. We'll be in touch to let you know when we launch.
                    </AnimateBody>
                </section>
            </Fragment>
        );
    }
}

export default Subscribed;
