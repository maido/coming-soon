/**
 * @prettier
 */
import React, {Component, Fragment} from 'react';
import GatsbyLink from 'gatsby-link';
import {easing, tween} from 'popmotion';
import posed from 'react-pose';
import config from '../../data/SiteConfig';
import Logo from '../components/Logo';
import NewsletterSignupForm from '../components/NewsletterSignupForm';
import SEO from '../components/SEO';

const INITIAL_DELAY = 800;
const ANIMATIONS = {
    hidden: {
        opacity: 0,
        translateY: 30
    },
    visible: {
        opacity: 1,
        transition: props => tween({...props, duration: 800, easing: easing.circOut}),
        translateY: 0
    }
};
const AnimatedLogo = posed.div(ANIMATIONS);
const AnimateBody = posed.div({
    hidden: {...ANIMATIONS.hidden, ...{translateY: 20}},
    visible: ANIMATIONS.visible
});

class PrivacyPolicy extends Component {
    constructor(props) {
        super(props);

        this.state = {animateBody: false, animateLogo: false};
    }

    componentDidMount() {
        setTimeout(() => this.setState({animateLogo: true}), 0);
        setTimeout(() => this.setState({animateBody: true}), 200);
    }

    render() {
        const {animateBody, animateLogo} = this.state;

        return (
            <Fragment>
                <SEO />
                <section className="o-wrapper u-max-w-620 u-pv">
                    <div className="u-max-w-400 u-m-auto u-text-center">
                        <AnimatedLogo
                            pose={animateLogo ? 'visible' : 'hidden'}
                            className="u-js-anim"
                        >
                            <Logo className="c-site-logo u-mb+" />
                        </AnimatedLogo>
                    </div>

                    <AnimateBody pose={animateBody ? 'visible' : 'hidden'} className="u-js-anim">
                        <GatsbyLink to="/" className="u-color-primary">
                            &larr; Home
                        </GatsbyLink>
                    </AnimateBody>
                </section>
            </Fragment>
        );
    }
}

export default PrivacyPolicy;
