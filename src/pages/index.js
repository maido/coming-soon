/**
 * @prettier
 */
import React, {Component, Fragment} from 'react';
import {easing, tween} from 'popmotion';
import posed from 'react-pose';
import config from '../../data/SiteConfig';
import Logo from '../components/Logo';
import NewsletterSignupForm from '../components/NewsletterSignupForm';
import SEO from '../components/SEO';

const INITIAL_DELAY = 800;
const ANIMATIONS = {
    hidden: {
        opacity: 0,
        translateY: 30
    },
    visible: {
        opacity: 1,
        transition: props => tween({...props, duration: 800, easing: easing.circOut}),
        translateY: 0
    }
};
const AnimatedHeadline = posed.div(ANIMATIONS);
const AnimatedLogo = posed.div(ANIMATIONS);
const AnimatedForm = posed.div({
    hidden: {...ANIMATIONS.hidden, ...{translateY: 20}},
    visible: ANIMATIONS.visible
});

class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {animateForm: false, animateHeadline: false, animateLogo: false};
    }

    componentDidMount() {
        setTimeout(() => this.setState({animateLogo: true}), 300);
        setTimeout(() => this.setState({animateHeadline: true}), 500);
        setTimeout(() => this.setState({animateForm: true}), 1200);
    }

    render() {
        const {animateForm, animateHeadline, animateLogo} = this.state;

        return (
            <Fragment>
                <SEO />
                <section className="o-wrapper u-max-w-620 u-text-center">
                    <div className="u-max-w-400 u-m-auto">
                        <AnimatedLogo
                            pose={animateLogo ? 'visible' : 'hidden'}
                            className="u-js-anim"
                        >
                            <Logo className="c-site-logo u-mb- u-mb@mobile" />
                        </AnimatedLogo>

                        <AnimatedHeadline
                            pose={animateHeadline ? 'visible' : 'hidden'}
                            className="u-js-anim"
                        >
                            <h1 className="u-heading-alternate u-block">
                                A new lifestyle brand for discerning dogs
                            </h1>
                        </AnimatedHeadline>
                    </div>

                    <AnimatedForm
                        pose={animateForm ? 'visible' : 'hidden'}
                        className="u-mt u-mt+@mobile u-js-anim"
                    >
                        <NewsletterSignupForm />
                    </AnimatedForm>
                </section>
            </Fragment>
        );
    }
}

export default Index;
