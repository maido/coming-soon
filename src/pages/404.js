/**
 * @prettier
 */
import React, {Fragment} from 'react';
import Helmet from 'react-helmet';
import config from '../../data/SiteConfig';

const Error404 = () => (
    <Fragment>
        <Helmet title={`Error – ${config.site.title}`} />

        <section className="o-wrapper o-section" />
    </Fragment>
);
export default Error404;
