/**
 * @prettier
 */
import React from 'react';
import GatsbyLink from 'gatsby-link';
import {OutboundLink} from 'gatsby-plugin-google-analytics';

const Link = ({children, to, ...other}) => {
    const internal = /^\/(?!\/)/.test(to);

    if (internal) {
        return (
            <GatsbyLink to={to} {...other}>
                {children}
            </GatsbyLink>
        );
    } else {
        if (to !== '') {
            return (
                <OutboundLink href={to} className={other.className}>
                    {children}
                </OutboundLink>
            );
        } else {
            return (
                <button type="button" {...other}>
                    {children}
                </button>
            );
        }
    }
};

export default Link;
