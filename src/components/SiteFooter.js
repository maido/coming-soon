/**
 * @prettier
 */
import React from 'react';
import config from '../../data/SiteConfig';
import Link from './Link';

const SiteFooter = ({handleNewsletterSignupClick}) => (
    <footer className="t-grey-lightest u-ph u-pv+ u-pv++@mobile c-site-footer" />
);

export default SiteFooter;
