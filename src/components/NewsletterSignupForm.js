/**
 * @prettier
 */
import 'node-fetch';
import React, {Component, Fragment} from 'react';
import map from 'lodash/map';
import Link from 'gatsby-link';
import {easing, tween} from 'popmotion';
import posed from 'react-pose';
import equals from 'validator/lib/equals';
import isEmail from 'validator/lib/isEmail';
import {DEFAULT_ANIMATION_OPTIONS} from '../helpers';
import config from '../../data/SiteConfig';
import CTAButton from './CTAButton';
import CustomCheckbox from './CustomCheckbox';
import FormField from './FormField';

const validate = (field, value) => {
    const validator = field.validator[0];
    const options = field.validator[1];
    const isValid = validator(value, options);

    return isValid;
};

const ANIMATIONS = {
    hidden: {
        opacity: 0,
        translateY: 20
    },
    visible: {
        opacity: 1,
        transition: props => tween({...props, duration: 800, easing: easing.circOut}),
        translateY: 0
    }
};

const AnimatedNameFields = posed.div(ANIMATIONS);

class NewsletterSignupForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                accept: {validator: [equals, 'yes']},
                EMAIL: {validator: [isEmail]}
            },
            fieldValues: {
                accept: '',
                EMAIL: ''
            },
            focused: [],
            touched: [],
            isValid: false
        };
    }

    isFormValid() {
        const hasErrors = Object.values(this.state.fieldValues).filter(
            value => value === '' || value === 'has-error'
        );

        return hasErrors.length === 0;
    }

    validateAllFields() {
        const touched = Object.keys(this.state.fieldValues);
        let fieldValues = {};

        map(this.state.fieldValues, (value, key) => {
            fieldValues[key] = value === '' || value === 'has-error' ? 'has-error' : value;
        });

        this.setState({fieldValues, touched});
    }

    updateFormState(field, value) {
        const fieldValues = {
            ...this.state.fieldValues,
            ...{[field]: value}
        };

        this.setState({fieldValues}, this.setFormValidity);
    }

    setFormValidity() {
        const isValid = this.isFormValid();

        this.setState({isValid});
    }

    handleFieldBlur(field) {
        const touched = [...this.state.touched, ...field];

        this.setState({touched}, () => {
            this.setFormValidity();
        });
    }

    handleFieldFocus(field) {
        const focused = [...this.state.focused, ...field];

        this.setState({focused});
    }

    handleFieldChange(field, event, value) {
        if (event) {
            value = event.target.value;
        }

        const isValid = validate(this.state.fields[field], value);

        this.updateFormState(field, isValid ? value : 'has-error');
    }

    handleRadioClick(field, value) {
        this.updateFormState(field, value);
    }

    handleFormSubmit(event) {
        if (!this.state.isValid) {
            event.preventDefault();
            this.validateAllFields();
        }
    }

    hasError(field) {
        return this.state.fieldValues[field] === 'has-error' && this.state.touched.includes(field);
    }

    render() {
        const {fields, fieldValues, isValid, touched} = this.state;
        const {handleDismiss, title} = this.props;

        return (
            <Fragment>
                <form
                    onSubmit={this.handleFormSubmit.bind(this)}
                    className="c-form u-mt"
                    action="https://maido.us2.list-manage.com/subscribe/post?u=991d1151df0c0988747182c17&amp;id=32f9a217ad"
                    method="post"
                    autoComplete="off"
                >
                    <div className="c-form__field">
                        <FormField
                            errorMessage="Add your email so we can message you when we launch"
                            hasError={this.hasError('EMAIL')}
                        >
                            <input
                                type="EMAIL"
                                id="EMAIL"
                                name="EMAIL"
                                maxLength="100"
                                className="c-form__input u-flex-grow u-1/1 u-depth"
                                placeholder="Enter your email address..."
                                onBlur={this.handleFieldBlur.bind(this, 'EMAIL')}
                                onFocus={this.handleFieldFocus.bind(this, 'EMAIL')}
                                onKeyUp={this.handleFieldChange.bind(this, 'EMAIL')}
                            />
                        </FormField>
                    </div>

                    <div className="u-mt u-text-left u-mb-@mobile u-font-size-13 u-color-grey">
                        <FormField
                            errorMessage="You must accept our terms before signing up"
                            hasError={this.hasError('accept')}
                        >
                            <CustomCheckbox
                                id="newsletter-consent"
                                name="accept"
                                onBlur={this.handleFieldBlur.bind(this, 'accept')}
                                onChange={this.handleFieldChange.bind(
                                    this,
                                    'accept',
                                    null,
                                    this.state.fieldValues.accept === 'yes' ? 'no' : 'yes'
                                )}
                            >
                                Tick here to receive news, offers and promotions from Barkism by
                                email. For more information about how we use your information, read
                                our full{' '}
                                <Link to="/privacy-policy" className="u-underline">
                                    privacy policy
                                </Link>.
                            </CustomCheckbox>
                        </FormField>
                    </div>

                    <CTAButton>Sign up</CTAButton>
                </form>
            </Fragment>
        );
    }
}

export default NewsletterSignupForm;
