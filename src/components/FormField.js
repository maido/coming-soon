/**
 * @prettier
 */
import React from 'react';

const FormField = ({classes = '', children, errorMessage = 'This field is required', hasError}) => (
    <div className={`u-1/1 ${classes} ${hasError ? 'has-error' : ''}`}>
        {children}
        {hasError ? (
            <div className="c-form__feedback u-mv-- u-fade-in-up">
                <svg
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    className="c-form__feedback-icon u-mr--"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <g fill="none" fillRule="evenodd">
                        <circle stroke="#EB2905" strokeWidth="1.5" cx="9" cy="9" r="8.25" />
                        <path
                            d="M9.96 4.492l-.384 5.916H8.412l-.384-5.916H9.96zm-1.908 7.656c0-.528.42-.96.936-.96.528 0 .96.432.96.96a.953.953 0 0 1-.96.948.94.94 0 0 1-.936-.948z"
                            fill="#EB2905"
                        />
                    </g>
                </svg>
                {errorMessage}
            </div>
        ) : (
            ''
        )}
    </div>
);

export default FormField;
