/**
 * @prettier
 */
import React from 'react';

const CustomCheckbox = ({children, id, ...inputProps}) => (
    <div className="u-relative">
        <input
            type="checkbox"
            id={`${id}-checkbox`}
            className="c-form__checkbox u-mr--"
            {...inputProps}
        />

        <label className="c-form__label u-1/1" htmlFor={`${id}-checkbox`}>
            {children}
        </label>
    </div>
);

export default CustomCheckbox;
