/**
 * @prettier
 */
import React, {Fragment} from 'react';
import imageURL from '../helpers/imgix';
import HeroImages from './HeroImages';
import ProjectHeader from './ProjectHeader';
import Caption from './Caption';
import HeroCTA from './HeroCTA';
import Quote from './Quote';
import LinkBanner from './LinkBanner';
import RevealOnScroll from './RevealOnScroll';
import TextBlock from './TextBlock';
import HTML5Video from './HTML5Video';
import VimeoVideo from './VimeoVideo';

const RTEContent = ({content = []}) => (
    <Fragment>
        {content.map((item, index) => {
            switch (item.type) {
                case 'project-header':
                    return <ProjectHeader key={index} {...item} />;
                    break;
                case 'hero-images':
                    return <HeroImages isTall={true} key={index} {...item} />;
                    break;
                case 'text-block':
                    return (
                        <div
                            key={index}
                            className={`o-section ${item.theme ? `t-${item.theme}` : ''}`}
                        >
                            <div className="o-wrapper o-wrapper--small">
                                <TextBlock {...item} />
                            </div>
                        </div>
                    );
                    break;
                case 'quote':
                case 'caption':
                    return (
                        <div key={index} className="o-wrapper o-wrapper--small o-section">
                            <RevealOnScroll>
                                {item.type === 'quote' ? (
                                    <Quote {...item.quote} />
                                ) : (
                                    <Caption {...item.caption} />
                                )}
                            </RevealOnScroll>
                        </div>
                    );
                    break;
                case 'video':
                    return (
                        <div key={index} className="o-wrapper o-wrapper--medium o-section">
                            <HTML5Video {...item.video} />
                        </div>
                    );
                    break;
                case 'vimeo-video':
                    return (
                        <div key={index} className="o-wrapper o-wrapper--medium o-section">
                            <VimeoVideo {...item} />
                        </div>
                    );
                    break;
                case 'image':
                    return (
                        <div key={index} className="o-wrapper o-wrapper--medium o-section">
                            <img src={imageURL(item.image)} alt="" className="u-1/1" />
                        </div>
                    );
                    break;
                case 'hero-cta':
                    return (
                        <HeroCTA
                            key={index}
                            {...item}
                            background={item.image}
                            text={item.description}
                        />
                    );
                    break;
                case 'link-banner':
                    return <LinkBanner key={index} {...item.cta} text={item.description} />;
                    break;
            }
        })}
    </Fragment>
);

export default RTEContent;
