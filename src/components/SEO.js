/**
 * @prettier
 */
import React, {Component} from 'react';
import Helmet from 'react-helmet';
import config from '../../data/SiteConfig';

class SEO extends Component {
    getMetaContent() {
        const defaultContent = {
            description: config.site.description,
            image: config.site.socialImage,
            title: config.site.title,
            url: config.site.url
        };
        const content = {...defaultContent, ...this.props};

        return content;
    }

    getSchemaOrgJSONLD() {
        const schema = {
            '@context': 'http://schema.org',
            '@type': 'Organization',
            name: config.site.title,
            alternateName: config.site.titleAlt,
            url: config.site.url,
            address: {
                '@type': 'PostalAddress',
                streetAddress: config.contact.address.street,
                postalCode: config.contact.address.postcode,
                addressLocality: config.contact.address.locality
            },
            logo: {
                '@type': 'ImageObject',
                contentUrl: config.site.logo
            },
            email: config.contact.email,
            telephone: config.contact.phone
        };

        return schema;
    }

    render() {
        const {description, image, title, url} = this.getMetaContent();
        const schemaOrgJSONLD = this.getSchemaOrgJSONLD();

        return (
            <Helmet>
                <meta name="description" content={description} />
                <meta name="image" content={image} />
                <title>
                    {title && title !== config.site.title ? `${title} | ` : ''}
                    {config.site.title}
                </title>

                <script type="application/ld+json">{JSON.stringify(schemaOrgJSONLD)}</script>

                <meta property="og:url" content={url} />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={description} />
                <meta property="og:image" content={image} />

                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:creator" content={config.social.twitterUsername} />
                <meta name="twitter:title" content={title} />
                <meta name="twitter:description" content={description} />
                <meta name="twitter:image" content={image} />
            </Helmet>
        );
    }
}

export default SEO;
