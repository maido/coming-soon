/**
 * @prettier
 */
import React from 'react';
import StaggeredAnimatedItems from './StaggeredAnimatedItems';

const PageHeader = ({headingSize = 1, text, title}) => {
    const Heading = `h${headingSize}`;

    return (
        <StaggeredAnimatedItems
            items={[
                <Heading
                    className={`u-heading-alternate u-title-wrapper u-h${headingSize} u-mb u-block`}
                >
                    {title}
                </Heading>,
                text ? <p className="u-color-grey u-lead u-mt">{text}</p> : null
            ]}
        />
    );
};

export default PageHeader;
