/**
 * @prettier
 */
import React, {Fragment} from 'react';
import Anime from 'react-anime';

const MOTION_DEFAULT_Y = 40;
const MOTION_DEFAULT_DELAY = 100;

const StaggeredAnimatedItems = ({items, vertical = true, wrapperClass = ''}) => (
    <Fragment>
        {items.map((item, index) => (
            <Anime
                autoplay={true}
                duration={2500}
                delay={index > 0 ? MOTION_DEFAULT_DELAY * (index * 0.5) : 0}
                easing={[0.91, -0.54, 0.29, 1.06]}
                opacity={[0, 1]}
                translateY={[MOTION_DEFAULT_Y * ((index + 1) * 1.15), 0]}
            >
                <div className={`${wrapperClass} u-js-anim`}>{item}</div>
            </Anime>
        ))}
    </Fragment>
);

export default StaggeredAnimatedItems;
