/**
 * @prettier
 */
import React, {Fragment} from 'react';
import LoadingDots from './LoadingDots';

const CTAButton = ({children, state}) => (
    <button className="c-button c-button--round c-button--primary u-mt u-case-upper u-1/1 u-w-auto@mobile">
        {state === 'loading' ? (
            <Fragment>
                &nbsp;<LoadingDots className="c-loading-dots" />
            </Fragment>
        ) : (
            children
        )}
    </button>
);

export default CTAButton;
