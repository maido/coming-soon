/**
 * @prettier
 */
import React, {Component} from 'react';
import config from '../../data/SiteConfig';

let inlinedStyles = '';

if (process.env.NODE_ENV === 'production') {
    try {
        inlinedStyles = require('!raw-loader!../public/styles.css');
    } catch (e) {
        console.log(e);
    }
}

const HTML = ({headComponents, body, postBodyComponents}) => {
    let css;

    if (process.env.NODE_ENV === 'production') {
        css = <style id="gatsby-inlined-css" dangerouslySetInnerHTML={{__html: inlinedStyles}} />;
    }

    return (
        <html lang="en">
            <head>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="description" content={config.site.description} />
                {this.props.headComponents}
                <link rel="shortcut icon" href={favicon} />
                {css}
            </head>
            <body>
                <div id="___gatsby" dangerouslySetInnerHTML={{__html: this.props.body}} />
                {this.props.postBodyComponents}
            </body>
        </html>
    );
};

export default HTML;
