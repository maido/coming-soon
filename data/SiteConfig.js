/**
 * @prettier
 */
module.exports = {
    site: {
        title: 'Barkism – A new lifestyle brand for discerning dogs',
        titleAlt: 'Barkism',
        logo: 'https://www.barkism.com/images/social.png',
        url: 'https://www.barkism.com',
        socialImage: 'https://www.barkism.com/images/social.jpg',
        description: 'A new lifestyle brand for discerning dogs',
        copyright: 'Copyright © 2018. Barkism',
        tagline: ''
    },
    navigation: {
        primary: [
            {
                title: 'Sign Up',
                url: '/'
            }
        ],
        secondary: [
            {
                title: 'Privacy policy',
                url: '/privacy-policy'
            }
        ]
    },
    tracking: {
        googleAnalyticsID: 'UA-119615879-1'
    },
    social: {
        twitterUsername: ''
    },
    contact: {
        email: 'hello@barkism.com',
        phone: '',
        address: {
            street: '27-33 Bethnal Green Rd',
            locality: 'London',
            postcode: 'E1 6LA'
        }
    }
};
